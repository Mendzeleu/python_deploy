#! /usr/bin/env python
# -*- coding: utf-8 -*-
import email
import imaplib

################Get Body Email################################################################


def checkMail(self, username, password):
    M = imaplib.IMAP4_SSL("VMS-EXCH.intervale.ru")
    try:
        M.login('feedback_sendmoney@intervale.ru', '000000Qq')
    except Exception as e:
        if "[AUTHENTICATIONFAILED]" in str(e):
            return None
        else:
            raise e
    M.select(readonly=True)
    messages = []
    typ, data = M.search(None, '(UNSEEN)', '(FROM "smendelev@intervale.ru")')
    for num in data[0].split():
        typ, data = M.fetch(num, '(RFC822)')
        for response_part in data:
            if isinstance(response_part, tuple):
                email_parser = email.parser.BytesFeedParser()
                email_parser.feed(response_part[1])
                msg = email_parser.close()
                messages.append({"HEADERS": {header.upper(): msg[header] for header in ['to', 'from', "subject"]}})
                body = ""
                if msg.is_multipart():
                    for part in msg.walk():
                        ctype = part.get_content_type()
                        cdispo = str(part.get('Content-Disposition'))
                        if ctype == 'text/plain' and 'attachment' not in cdispo:
                            body = part.get_payload(decode=True)
                            break
                else:
                    body = msg.get_payload(decode=True)
                messages[-1].update({"BODY": body.decode()})

    M.close()
    M.logout()
    return messages