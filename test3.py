#! /usr/bin/env python
# -*- coding: utf-8 -*-
import email
import imaplib
import re
import openpyxl
import datetime
from openpyxl.styles import Alignment

################Get Body Email################################################################

mail = imaplib.IMAP4_SSL('VMS-EXCH.intervale.ru')
mail.login('feedback_sendmoney@intervale.ru', '000000Qq')
mail.list()
mail.select("inbox")

typ, data = mail.search(None, '(UNSEEN)', '(FROM "smendelev@intervale.ru")')
mail_ids = data[0]
id_list = mail_ids.split()


#iterate through 15 messages in decending order starting with latest_email_id
#the '-1' dictates reverse looping order
for i in range(id_list, id_list-15, -1):
   typ, data = mail.fetch(i, '(RFC822)')

   for response_part in data:
      if isinstance(response_part, tuple):
          msg = email.message_from_string(response_part[1])
          varSubject = msg['subject']
          varFrom = msg['from']

   #remove the brackets around the sender email address
   varFrom = varFrom.replace('<', '')
   varFrom = varFrom.replace('>', '')

   #add ellipsis (...) if subject length is greater than 35 characters
   if len( varSubject ) > 35:
      varSubject = varSubject[0:32] + '...'

   print('[' + varFrom.split()[-1] + '] ' + varSubject)

mail.close()
mail.logout()