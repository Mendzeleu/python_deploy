#! /usr/bin/env python
# -*- coding: utf-8 -*-
import email
import imaplib
import re
import openpyxl
import datetime
from openpyxl.styles import Alignment

################Get Body Email################################################################

mail = imaplib.IMAP4_SSL('VMS-EXCH.intervale.ru')
mail.login('feedback_sendmoney@intervale.ru', '000000Qq')
mail.list()
mail.select("inbox")
typ, data = mail.search(None, '(UNSEEN)', '(FROM "smendelev@intervale.ru")')
mail_ids = data[0]
id_list = mail_ids.split()

for num in data[0].split():
  typ, data = mail.fetch(num, '(RFC822)' )
  raw_email = data[0][1]

  raw_email_string = raw_email.decode('utf-8')
  email_message = email.message_from_string(raw_email_string)

  for response_part in data:
    if isinstance(response_part, tuple):
      msg = email.message_from_string(response_part[1].decode('utf-8'))
      email_date = msg['Date']
      email_subject = msg['subject']
      email_from = msg['from']
      body_content = msg.get_payload(decode=True)
#print(email_date)
      otzyv = re.findall(r'.*?Отзыв: (.*?)<\/p>', body_content, re.M|re.I) or ['']
#print(otzyv[0])
      platform = re.findall(r'.*?platform: (.*?)<br>', body_content, re.M | re.I) or ['']
#print(platform[0])
      model = re.findall(r'.*?model: (.*?)<br>', body_content, re.M | re.I) or ['']
#print(model[0])
      app_ver = re.findall(r'.*?app_ver: (.*?)<br>', body_content, re.M | re.I) or ['']
#print(app_ver[0])
      login = re.findall(r'.*?login: (.*?)<br>', body_content, re.M | re.I) or ['']
#print(login[0])
      mark = re.findall(r'.*?mark: (.*?)<\/p>', body_content, re.M | re.I) or ['']
#print(mark[0])

mail.close()
mail.logout()